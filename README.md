# TODO Check list

[X] Implement the design

[X] Use semantic HTML

[X] Consider accessibility

[X] Support latest Chrome, Safari, Firefox, Edge browsers and IE 11+

[X] Mobile friendly

[X] Don't use any other component library / framework

[X] The cover can be placed above or under the header

[X] They are responsive and can be used as a single element or in grid system

[X] When grid system, they are stretched to the same height

[X] Card have skeleton design

[X] The can also be clicked (not mandatory)

[X] Loading state

[X] Default state

[X] Hover / Active state

[X] The cover image can be an absolute height, or can keep the image ratio

[X] Card anatomy: Cover, Thumbnail, Title, Subtitle, Body, Footer

[X] The card options can appear in any combination

[X] We would like to see 4 different card variations

[X] One card must display every option

[X] Make sure we can see the loading state

[X] Make sure to implement different click actions

[X] Make sure this page is responsive

[X] For the project use Vue/Nuxt.js as your framework. 
# VUE Card Component

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
